using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float moveSpeed = 10f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    private Vector3 moveDirection;

    private bool onGround = true;

    private bool canDoubleJump = true;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision coll)
    {
        onGround = true;
        canDoubleJump = true;
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        //if (Input.GetButton("Fire3") == true)
        //{
        //    //m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude * 2);
        //    //m_Rigidbody.MoveRotation(m_Rotation);
        //}
        //else
        //{
        //    if (Input.GetKey(KeyCode.W))
        //    {
        //        transform.Translate(Vector3.forward * 10 * Time.deltaTime);
        //        var rotationVector = transform.rotation.eulerAngles;
        //        rotationVector.x = 0;
        //        transform.rotation = Quaternion.Euler(rotationVector);
        //    }
        //    else if (Input.GetKey(KeyCode.S)) 
        //    {
        //        transform.Translate(Vector3.forward * 10 * Time.deltaTime);
        //        var rotationVector = transform.rotation.eulerAngles;
        //        rotationVector.x = 180;
        //        transform.rotation = Quaternion.Euler(rotationVector);
        //    }



    }

	void Update()
    {
        //Vector2 playerInput;
        //playerInput.x = Input.GetAxis("Horizontal");
        //playerInput.y = Input.GetAxis("Vertical");
        //playerInput = Vector2.ClampMagnitude(playerInput, 1f);
        //float maxSpeed = 10f;
        //Vector3 velocity =
        //    new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        //Vector3 displacement = velocity * Time.deltaTime;
        //transform.localPosition += displacement;

        //moveDirection = (transform.forward * Input.GetAxis("Vertical") * moveSpeed) + (transform.right * Input.GetAxis("Horizontal") * moveSpeed);
        if (Input.GetButton("Fire3") == true)
        {
            Vector3 moveDirectionForward = this.transform.forward * Input.GetAxis("Vertical");
            Vector3 moveDirectionRight = this.transform.right * Input.GetAxis("Horizontal");
            Vector3 totalMove = moveDirectionForward * (moveSpeed * 2) + moveDirectionRight * (moveSpeed * 2) + new Vector3(0f, m_Rigidbody.velocity.y, 0f);
            m_Rigidbody.velocity = totalMove; //new Vector3(Input.GetAxis("Horizontal") * moveSpeed, m_Rigidbody.velocity.y, Input.GetAxis("Vertical") * moveSpeed);
        }
        else 
        {
            Vector3 moveDirectionForward = this.transform.forward * Input.GetAxis("Vertical");
            Vector3 moveDirectionRight = this.transform.right * Input.GetAxis("Horizontal");
            Vector3 totalMove = moveDirectionForward * moveSpeed + moveDirectionRight * moveSpeed + new Vector3(0f, m_Rigidbody.velocity.y, 0f);
            m_Rigidbody.velocity = totalMove; //new Vector3(Input.GetAxis("Horizontal") * moveSpeed, m_Rigidbody.velocity.y, Input.GetAxis("Vertical") * moveSpeed);
        }



        if (Input.GetButtonDown("Jump"))
        {
            if (onGround == true)
            {
                m_Rigidbody.AddForce(new Vector3(0f, 300f, 0f));
                onGround = false;
            }
            else if (canDoubleJump == true) 
            {
                m_Rigidbody.AddForce(new Vector3(0f, 300f, 0f));
                onGround = false;
                canDoubleJump = false;
            }
        }
    }

void OnAnimatorMove()
    {
        

    }
}
